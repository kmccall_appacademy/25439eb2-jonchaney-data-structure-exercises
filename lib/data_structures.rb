# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.sort.last - arr.sort.first
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr.sort == arr
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  str.chars.count {|letter| vowel?(letter.downcase)}
end

def vowel?(letter)
  "aeiou".include?(letter)
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.chars.map {|letter| letter if !vowel?(letter.downcase)}.join
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.chars.map {|el| el.to_i}.sort.reverse.map {|el| el.to_s}
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  i = 0
  while i < str.length-1
    return true if str[i].downcase == str[i+1].downcase
    i+=1
  end
  false
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  "(#{arr[0]}#{arr[1]}#{arr[2]}) #{arr[3]}#{arr[4]}#{arr[5]}-#{arr[6]}#{arr[7]}#{arr[8]}#{arr[9]}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  range(str.split(",").map {|num| num.to_i})
end

#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  arr.drop(offset%arr.length) + arr.take(offset%arr.length)
end
